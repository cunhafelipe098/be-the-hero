const express = require('express');
const bodyParser = require('body-parser');
const { errors } = require('celebrate');
const routes = require('./routes');
const cors = require('cors');



const app = express();

app.use(cors());
app.use(bodyParser.text());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(routes)
app.use(errors());

module.exports = app;