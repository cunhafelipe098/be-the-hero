// Axios é o responsável por comunicar com algun serviso externo 

import axios from 'axios';

const api = axios.create({
    baseURL: 'http://localhost:3333',
});

export default api